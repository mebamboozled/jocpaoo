package PaooGame.Object;

import PaooGame.Entity.Player;
import PaooGame.GameWindow.GameWindow;
import PaooGame.Tiles.Tile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.Buffer;

import static PaooGame.Tiles.Tile.TILE_HEIGHT;
import static PaooGame.Tiles.Tile.TILE_WIDTH;

public class SuperObject {
    public Tile Obj;
    public int worldX;
    public int worldY;
    public boolean collision;
    public String name;
    BufferedImage image;
    int id;
    public Rectangle solidArea;
    int angle = 0;

    public void drawFirstHalf(Graphics g, Player player)
    {
        int screenX = worldX - player.worldX + player.screenX;
        int screenY = worldY - player.worldY + player.screenY;
        if(worldX >  player.worldX - player.screenX - TILE_WIDTH * 2 && worldX < player.worldX + player.screenX + TILE_WIDTH * 2 && worldY > player.worldY - player.screenY - TILE_HEIGHT * 2 && screenY <= player.screenY + TILE_HEIGHT/2 + 8) {
            g.drawImage(Obj.img.b, screenX, screenY, Obj.width, Obj.height, null);
        }

    }
    public void drawSecondHalf(Graphics g, Player player)
    {
        int screenX = worldX - player.worldX + player.screenX;
        int screenY = worldY - player.worldY + player.screenY;
        if(worldX >  player.worldX - player.screenX - TILE_WIDTH * 2 && worldX < player.worldX + player.screenX + TILE_WIDTH * 2 && screenY > player.screenY + TILE_HEIGHT/2 + 8 && worldY < (player.worldY + player.screenY + TILE_HEIGHT * 2)) {
            g.drawImage(Obj.img.b, screenX, screenY, Obj.width, Obj.height, null);
        }

    }
        }
