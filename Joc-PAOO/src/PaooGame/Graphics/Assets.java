package PaooGame.Graphics;

import PaooGame.Object.OBJ_Chest;
import PaooGame.Object.OBJ_Key;
import PaooGame.Tiles.Tile;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*! \class public class Assets
    \brief Clasa incarca fiecare element grafic necesar jocului.

    Game assets include tot ce este folosit intr-un joc: imagini, sunete, harti etc.
 */
public class Assets
{
        /// Referinte catre elementele grafice (dale) utilizate in joc.
    public static MyBuffImg soil;
    public static MyBuffImg air;
    public static MyBuffImg player;
    public static MyBuffImg[] allTiles = new MyBuffImg[200];
    public static SpriteSheet sheet = new SpriteSheet(ImageLoader.LoadImage("/textures/spriteSheet.png"));
    public static int nrTiles = 0;
    /*! \fn public static void Init()
        \brief Functia initializaza referintele catre elementele grafice utilizate.

        Aceasta functie poate fi rescrisa astfel incat elementele grafice incarcate/utilizate
        sa fie parametrizate. Din acest motiv referintele nu sunt finale.
     */

    public static void Init() {

        /// Se creaza temporar un obiect SpriteSheet initializat prin intermediul clasei ImageLoader
        air = new MyBuffImg();
        soil = new MyBuffImg();
        player = new MyBuffImg();
        /*System.out.println(ImageLoader.LoadImage("/textures/0x72_DungeonTilesetII_v14.png").getType());*/
        /// Se obtin subimaginile corespunzatoare elementelor necesare.
        air = sheet.crop("air 16 400 16 16");
        soil = sheet.crop("floor_1 16 64 16 16");
        player = sheet.crop("wizzard_m_idle_anim 128 164 16 28 4");
        nrTiles = readAllTiles();
        Tile.loadAllTiles(allTiles, nrTiles);
        setObject();
    }

    public static void setObject()
    {
           Tile.obj[0] = new OBJ_Key(164);
           Tile.obj[0].worldX = 21 * Tile.TILE_WIDTH;
           Tile.obj[0].worldY = 15 * Tile.TILE_HEIGHT - Tile.TILE_HEIGHT/2;
           Tile.obj[1] = new OBJ_Key(164);
           Tile.obj[1].worldX = 30 * Tile.TILE_WIDTH;
           Tile.obj[1].worldY = 23 * Tile.TILE_HEIGHT - Tile.TILE_HEIGHT/2;
           Tile.obj[1] = new OBJ_Chest(65);
           Tile.obj[1].worldX = 10 * Tile.TILE_WIDTH;
           Tile.obj[1].worldY = 10 * Tile.TILE_HEIGHT;
    }

    public static String randTile(String numeTile)
    {
        List<String> lista = new ArrayList();
        Pattern ceCaut = Pattern.compile(numeTile + "_[0-9]*", Pattern.CASE_INSENSITIVE);
        Matcher matcher;
        boolean matchFound;
        try {
            File myObj = new File(new File("tiles_list_v14.txt").getAbsolutePath());
            Scanner myReader = new Scanner(myObj);
            while(myReader.hasNextLine())
            {
                String data = myReader.nextLine();
                matcher = ceCaut.matcher(data);
                matchFound = matcher.find();
                if(matchFound)
                {
                    lista.add(data);
                }
            }
            myReader.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return lista.get((int)(Math.random()* lista.size()));
    }

    public static int readAllTiles()
    {
        int nrTiles = 1;
        try {
            File myObj = new File(new File("tiles_list_v14.txt").getAbsolutePath());
            Scanner myReader = new Scanner(myObj);
            while(myReader.hasNextLine())
            {
                String data = myReader.nextLine();
                if(data != "") {
                    allTiles[nrTiles] = new MyBuffImg(sheet.cropNoAnim(data));
                    allTiles[nrTiles].reference = data;
                    nrTiles++;
                }
            }
            myReader.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return nrTiles;
    }
}


