package PaooGame.Input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyHandler implements KeyListener {

    public boolean upPressed, downPressed, leftPressed, rightPressed, spacePressed;
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        switch (code)
        {
            case KeyEvent.VK_W:
                upPressed = true;
                return;
            case KeyEvent.VK_S:
                downPressed = true;
                return;
            case KeyEvent.VK_A:
                leftPressed = true;
                return;
            case KeyEvent.VK_D:
                rightPressed = true;
                return;
            case KeyEvent.VK_SPACE:
                spacePressed = true;
                return;
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        int code = e.getKeyCode();
        switch (code)
        {
            case KeyEvent.VK_W:
                upPressed = false;
                return;
            case KeyEvent.VK_S:
                downPressed = false;
                return;
            case KeyEvent.VK_A:
                leftPressed = false;
                return;
            case KeyEvent.VK_D:
                rightPressed = false;
                return;
            case KeyEvent.VK_SPACE:
                spacePressed = false;
                return;
        }
    }
}
