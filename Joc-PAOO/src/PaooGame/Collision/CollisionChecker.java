package PaooGame.Collision;

import PaooGame.Entity.Entity;
import PaooGame.GameWindow.GameWindow;
import PaooGame.Object.SuperObject;
import PaooGame.Tiles.Tile;

import java.awt.*;

import static PaooGame.Tiles.Tile.TILE_HEIGHT;
import static PaooGame.Tiles.Tile.TILE_WIDTH;

public class CollisionChecker {
    GameWindow wnd;
    boolean colX = false, colY = false;
    Rectangle rec1, rec2;
    public CollisionChecker(GameWindow wnd)
    {
        this.wnd = wnd;
    }

    public void checkTile(Entity entity)
    {
        colX = false;
        colY = false;
        setCollision(entity, entity.solidAreaX, entity.solidAreaY);
        entity.collisionXOn = colX;
        entity.collisionYOn = colY;

    }



    public void setCollision(Entity entity, Rectangle rectangleX, Rectangle rectangleY)
    {
        int entityLeftWorldX = entity.worldX + rectangleX.x;
        int entityRightWorldX = entity.worldX + rectangleX.x + rectangleX.width;
        int entityTopWorldY = entity.worldY + rectangleY.y;
        int entityBottomWorldY = entity.worldY + rectangleY.y + rectangleY.height;

        int entityLeftCol = entityLeftWorldX / TILE_WIDTH - 7;
        int entityRightCol = entityRightWorldX/ TILE_WIDTH - 7;
        int entityTopRow = entityTopWorldY / TILE_HEIGHT;
        int entityBottomRow = entityBottomWorldY /  - 6;



        int tileNum1, tileNum2;
        rec1 = new Rectangle(entity.solidAreaX);
        rec2 = new Rectangle(entity.solidAreaY);
        if(entity.speedY < 0) {

            entityTopRow = (int) ((entityTopWorldY - entity.screenY) / TILE_HEIGHT);
            entityBottomRow = (int) ((entityBottomWorldY - entity.screenY) / TILE_HEIGHT);

            int worldXR = entityRightCol * TILE_WIDTH;
            int worldXL = entityLeftCol * TILE_WIDTH;
            int worldY = entityTopRow * TILE_HEIGHT;

            int screenXR = worldXR + entity.screenX - entity.worldX;
            int screenXL = worldXL + entity.screenX - entity.worldX;
            int screenY = worldY + entity.screenY - entity.worldY;

            tileNum1 = Tile.mapTileNum[1][entityRightCol][entityTopRow];
            tileNum2 = Tile.mapTileNum[1][entityLeftCol][entityTopRow];

            if (tileNum1 == -1)
                tileNum1 = 163;
            if (tileNum2 == -1)
                tileNum2 = 163;

            rec1 = new Rectangle(Tile.tiles[tileNum1].img.solidArea);
            rec2 = new Rectangle(Tile.tiles[tileNum2].img.solidArea);


            if((tileNum1 == 38 || tileNum1 == 39) && Tile.mapTileNum[1][entityRightCol][entityTopRow-1] == tileNum1 - 2)
            {
                rec1 = new Rectangle(Tile.tiles[tileNum1 - 2].img.solidArea);
            }
            if((tileNum2 == 38 || tileNum2 == 39) && Tile.mapTileNum[1][entityLeftCol][entityTopRow-1] == tileNum2 - 2)
            {
                rec2 = new Rectangle(Tile.tiles[tileNum2 - 2].img.solidArea);
            }


            //System.out.println(entityBottomRow + " L:" + entityLeftCol + " " + tileNum1 + " R:" + entityRightCol + " " + tileNum2);


            rec1.x += screenXR;
            rec1.y += screenY;
            rec2.x += screenXL;
            rec2.y += screenY;
            if ((entity.getBoundsY().intersects(rec1) && Tile.tiles[tileNum1].IsSolid())) {
                System.out.println("colliding");
                colY = true;
                entity.speedY = 0;
                //entity.worldY = -rec1.y + worldY + entity.screenY;
            }
            if ((entity.getBoundsY().intersects(rec2) && Tile.tiles[tileNum2].IsSolid())) {
                System.out.println("colliding");
                colY = true;
                entity.speedY = 0;
                //entity.worldY = -rec2.y + worldY + entity.screenY;
            }
        }
        else if(entity.speedY > 0){
            entityTopRow = (int) ((entityTopWorldY) / TILE_HEIGHT);
            entityBottomRow = (int) ((entityBottomWorldY - entity.screenY) / TILE_HEIGHT);

            int worldXR = entityRightCol * TILE_WIDTH;
            int worldXL = entityLeftCol * TILE_WIDTH;
            int worldY = entityBottomRow * TILE_HEIGHT;

            int screenXR = worldXR + entity.screenX - entity.worldX;
            int screenXL = worldXL + entity.screenX - entity.worldX;
            int screenY = worldY + entity.screenY - entity.worldY;

            tileNum1 = Tile.mapTileNum[1][entityRightCol][entityBottomRow];
            tileNum2 = Tile.mapTileNum[1][entityLeftCol][entityBottomRow];

            if(tileNum1 == -1)
                tileNum1 = 163;
            if(tileNum2 == -1)
                tileNum2 = 163;

            rec1 = new Rectangle(Tile.tiles[tileNum1].img.solidArea);
            rec2 = new Rectangle(Tile.tiles[tileNum2].img.solidArea);

            if((tileNum1 == 38 || tileNum1 == 39) && Tile.mapTileNum[1][entityRightCol][entityBottomRow-1] == tileNum1 - 2)
            {
                rec1 = new Rectangle(Tile.tiles[tileNum1 - 2].img.solidArea);
            }
            if((tileNum2 == 38 || tileNum2 == 39) && Tile.mapTileNum[1][entityLeftCol][entityBottomRow-1] == tileNum2 - 2)
            {
                rec2 = new Rectangle(Tile.tiles[tileNum2 - 2].img.solidArea);
            }


            rec1.x += screenXR;
            rec1.y += screenY;
            rec2.x += screenXL;
            rec2.y += screenY;
            if ((entity.getBoundsY().intersects(rec1) && Tile.tiles[tileNum1].IsSolid())) {
                colY = true;
                entity.speedY = 0;
                entity.worldY = -rec1.y + worldY + entity.screenY + (TILE_HEIGHT - rec1.height);
                }
            if (entity.getBoundsY().intersects(rec2) && Tile.tiles[tileNum2].IsSolid())
            {
                colY = true;
                entity.speedY = 0;
                entity.worldY = -rec2.y + worldY + entity.screenY + (TILE_HEIGHT- rec2.height);
            }
        }

        if(entity.speedX < 0){
                entityTopRow = (int) ((entityTopWorldY - entity.screenY) / TILE_HEIGHT);
                entityBottomRow = (int) ((entityBottomWorldY - entity.screenY) / TILE_HEIGHT);

                int worldYR = entityTopRow * TILE_HEIGHT;
                int worldYL = entityBottomRow * TILE_HEIGHT;
                int worldX = entityLeftCol * TILE_WIDTH;

                int screenYR =worldYR + entity.screenY - entity.worldY;
                int screenYL =worldYL + entity.screenY - entity.worldY;
                int screenX =worldX + entity.screenX - entity.worldX;

                tileNum1 = Tile.mapTileNum[1][entityLeftCol][entityTopRow];
                tileNum2 = Tile.mapTileNum[1][entityLeftCol][entityBottomRow];

                if(tileNum1 == -1)
                    tileNum1 = 163;
                if(tileNum2 == -1)
                    tileNum2 = 163;

                rec1 = new Rectangle(Tile.tiles[tileNum1].img.solidArea);
                rec2 = new Rectangle(Tile.tiles[tileNum2].img.solidArea);

                if((tileNum1 == 38 || tileNum1 == 39) && Tile.mapTileNum[1][entityLeftCol][entityTopRow-1] == tileNum1 - 2)
                {
                    rec1 = new Rectangle(Tile.tiles[tileNum1 - 2].img.solidArea);
                }
                if((tileNum2 == 38 || tileNum2 == 39) && Tile.mapTileNum[1][entityLeftCol][entityBottomRow-1] == tileNum2 - 2)
                {
                    rec2 = new Rectangle(Tile.tiles[tileNum2 - 2].img.solidArea);
                }


                rec1.x += screenX;
                rec1.y += screenYR;
                rec2.x += screenX;
                rec2.y += screenYL;
                rec1.height-=2;
                rec2.height-=2;
                if ((entity.getBoundsX().intersects(rec1) && Tile.tiles[tileNum1].IsSolid())) {
                    colX = true;
                    entity.speedX = 0;
                    entity.worldX = -rec1.x + worldX + entity.screenX;
                }
                if ((entity.getBoundsX().intersects(rec2) && Tile.tiles[tileNum2].IsSolid())) {
                    colX = true;
                    entity.speedX = 0;
                    entity.worldX = -rec2.x + worldX + entity.screenX;
                }
            }
            else if(entity.speedX > 0) {
                entityTopRow = (int) ((entityTopWorldY - entity.screenY) / TILE_HEIGHT);
                entityBottomRow = (int) ((entityBottomWorldY - entity.screenY) / TILE_HEIGHT);

                int worldYR = entityTopRow * TILE_WIDTH;
                int worldYL = entityBottomRow * TILE_WIDTH;
                int worldX = entityRightCol * TILE_HEIGHT;

                int screenYR =worldYR + entity.screenY - entity.worldY;
                int screenYL =worldYL + entity.screenY - entity.worldY;
                int screenX =worldX + entity.screenX - entity.worldX;

                tileNum1 = Tile.mapTileNum[1][entityRightCol][entityTopRow];
                tileNum2 = Tile.mapTileNum[1][entityRightCol][entityBottomRow];

                if(tileNum1 == -1)
                    tileNum1 = 163;
                if(tileNum2 == -1)
                    tileNum2 = 163;

                rec1 = new Rectangle(Tile.tiles[tileNum1].img.solidArea);
                rec2 = new Rectangle(Tile.tiles[tileNum2].img.solidArea);

                if((tileNum1 == 38 || tileNum1 == 39) && Tile.mapTileNum[1][entityRightCol][entityTopRow-1] == tileNum1 - 2)
                {
                    rec1 = new Rectangle(Tile.tiles[tileNum1 - 2].img.solidArea);
                }
                if((tileNum2 == 38 || tileNum2 == 39) && Tile.mapTileNum[1][entityRightCol][entityBottomRow-1] == tileNum2 - 2)
                {
                    rec2 = new Rectangle(Tile.tiles[tileNum2 - 2].img.solidArea);
                }


                rec1.x += screenX;
                rec1.y += screenYR;
                rec2.x += screenX;
                rec2.y += screenYL;
                rec1.height -= 2;
                rec2.height -= 2;
                if ((entity.getBoundsX().intersects(rec1) && Tile.tiles[tileNum1].IsSolid())) {
                    colX = true;
                    entity.speedX = 0;
                    entity.worldX = -rec1.x + worldX + entity.screenX  + (TILE_WIDTH - rec1.width);
                }
                if ((entity.getBoundsX().intersects(rec2) && Tile.tiles[tileNum2].IsSolid())) {
                    colX = true;
                    entity.speedX = 0;
                    entity.worldX = -rec2.x + worldX + entity.screenX + (TILE_WIDTH - rec2.width);
                }
            }
    }
    public void fillRec(Graphics g)
    {
        Graphics2D g2d = (Graphics2D) g;
        g2d.fill(rec1);
        g2d.fill(rec2);
    }
}


