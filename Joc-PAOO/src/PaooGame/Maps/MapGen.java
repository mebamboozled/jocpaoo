package PaooGame.Maps;

import com.sun.source.tree.Tree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class MapGen {
    public static int noMaps = 1;

    public void gen(){
    }

    public MapNode createNewNode() throws FileNotFoundException {
        int randId = (int) ((Math.random() * noMaps));
        MapNode newNode = new MapNode();


        File file = new File("res/maps/getMaps.txt");
        Scanner scanner = new Scanner(file);
        while(scanner.hasNextLine())
        {
            List<String> s = new ArrayList(Arrays.asList(scanner.nextLine().split(" ")));
            if(Integer.parseInt(s.get(0)) == randId)
            {
                newNode.mapId = randId;
                newNode.mapName = s.get(1);
                newNode.noLayers = Integer.parseInt(s.get(2));
                newNode.width = Integer.parseInt(s.get(3));
                newNode.height = Integer.parseInt(s.get(4));
            }
        }
        return newNode;
    }
}
