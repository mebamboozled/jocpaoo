package PaooGame.Tiles;

import PaooGame.Entity.Player;
import PaooGame.GameWindow.GameWindow;
import PaooGame.Graphics.MyBuffImg;
import PaooGame.Object.OBJ_Key;
import PaooGame.Object.SuperObject;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;

/*! \class public class Tile
    \brief Retine toate dalele intr-un vector si ofera posibilitatea regasirii dupa un id.
 */
public class Tile
{
    public static final int scale = 3;
    private static final int NO_TILES   = 200;
    public static Tile[] tiles          = new Tile[NO_TILES];       /*!< Vector de referinte de tipuri de dale.*/
    public static int nrTiles;
    /// De remarcat ca urmatoarele dale sunt statice si publice. Acest lucru imi permite sa le am incarcate
    /// o singura data in memorie
    /*    public static Tile grassTile        = new GrassTile(0);     *//*!< Dala de tip iarba*//*
    public static Tile mountainTile     = new MountainTile(1);  *//*!< Dala de tip munte/piatra*//*
    public static Tile waterTile        = new WaterTile(2);     *//*!< Dala de tip apa*//*
    public static Tile treeTile         = new TreeTile(3);      *//*!< Dala de tip copac*/
    public static Tile soilTile         = new SoilTile(1);      /*!< Dala de tip sol/pamant*/
    public static Tile airTile          = new AirTile(0);

    public static final int TILE_WIDTH  = 16 * scale;                       /*!< Latimea unei dale.*/
    public static final int TILE_HEIGHT = 16 * scale;                       /*!< Inaltimea unei dale.*/

    public int width;
    public int height;
    public MyBuffImg img;                                    /*!< Imaginea aferenta tipului de dala.*/
    protected final int id;                                         /*!< Id-ul unic aferent tipului de dala.*/
    static GameWindow gw = new GameWindow();
    static int nrLayers = 3;
    public static int[][][] mapTileNum = new int[nrLayers][gw.maxWolrdCol][gw.maxWolrdRow];
    String[] mapNames = {"res/map/MapLayer0.txt", "res/map/MapLayer1.txt", "res/map/MapLayer2.txt"};
    boolean collision = false;
    Vector<Integer> solidTiles = new Vector();
    public static SuperObject[] obj = new SuperObject[10];

    /*! \fn public Tile(BufferedImage texture, int id)
        \brief Constructorul aferent clasei.

        \param image Imaginea corespunzatoare dalei.
        \param id Id-ul dalei.
     */
    public Tile(BufferedImage image, int idd, int[] box)
    {
        img = new MyBuffImg (image);
        height = box[3] * scale;
        width = box[2] * scale;
        id = idd;
        tiles[id] = this;

        loadMaps(mapNames);
    }
    public Tile(MyBuffImg image, int idd)
    {
        img = image;
        height = image.box[3] * scale;
        width = image.box[2] * scale;
        id = idd;
    }
    public Tile(int idd)
    {
        img = tiles[idd].img;
        height = tiles[idd].img.box[3] * scale;
        width = tiles[idd].img.box[2] * scale;

        id = idd;
    }


    public static void loadAllTiles(MyBuffImg[] allTiles, int nr)
    {
        int contor = 0;
        nrTiles = nr;
        try {
            for (int i = 1; i < nrTiles; i++) {
                tiles[i] = new Tile(allTiles[i], i);
                contor++;
            }
        } catch (Exception e) {
            System.out.println("tile " + contor);
            e.printStackTrace();
        }

    }

    public Tile()
    {
        id = -1;
        loadMaps(mapNames);
        initSolid();
    }
    public Tile(Tile tile)
    {
        img = tile.img;
        height = tile.img.box[3] * scale;
        width = tile.img.box[2] * scale;
        id = tile.id;
    }

    /*! \fn public void Update()
        \brief Actualizeaza proprietatile dalei.
     */
    public void initSolid()
    {
        for(int i = 4; i < 17; i++)
        {
            solidTiles.add(i);
        }
        for(int i = 38; i < 62; i++)
        {
            solidTiles.add(i);
        }
        tiles[163].collision = true;
        for(int i = 0; i < solidTiles.size(); i++)
        {
            tiles[solidTiles.get(i)].collision = true;
        }
    }
    public void Update()
    {

    }

    /*! \fn public void Draw(Graphics g, int x, int y)
        \brief Deseneaza in fereastra dala.

        \param g Contextul grafic in care sa se realizeze desenarea
        \param x Coordonata x in cadrul ferestrei unde sa fie desenata dala
        \param y Coordonata y in cadrul ferestrei unde sa fie desenata dala
     */
    public void Draw(Graphics g, int x, int y) {
        /// Desenare dala

        g.drawImage(img.b, x, y, width, height, null);

    }
    public void DrawFlipped(Graphics g, int x, int y) {
        /// Desenare dala

        g.drawImage(img.b, x + TILE_WIDTH, y, -width, height, null);

    }

    public void Draw(Graphics g, Player player, int layerNr) {
        int worldCol = 0;
        int worldRow = 0;

        while(worldCol < gw.maxWolrdCol && worldRow < gw.maxWolrdRow)
        {
            int tileNum = mapTileNum[layerNr][worldCol][worldRow];

            int worldX = worldCol * TILE_WIDTH;
            int worldY = worldRow * TILE_HEIGHT;

            int screenX = worldX - player.worldX + player.screenX;
            int screenY = worldY - player.worldY + player.screenY;
            if(worldX >  player.worldX - player.screenX - TILE_WIDTH * 2 && worldX < player.worldX + player.screenX + TILE_WIDTH * 2 && worldY > player.worldY - player.screenY - TILE_HEIGHT * 2 && worldY < player.worldY + player.screenY + TILE_HEIGHT * 2)
                if(tileNum != -1)
                    g.drawImage(tiles[tileNum].img.b, screenX, screenY, tiles[tileNum].width, tiles[tileNum].height, null);
                else
                    g.drawImage(tiles[0].img.b, screenX, screenY, tiles[0].width, tiles[0].height, null);
            worldCol++;

            if(worldCol == gw.maxWolrdCol){
                worldCol = 0;
                worldRow++;
            }
        }
    }
/*    public void DrawFirstHalf(Graphics g, Player player, int layerNr) {
        int worldCol = 0;
        int worldRow = 0;

        while (worldCol < gw.maxWolrdCol && worldRow < gw.maxWolrdRow) {
            int tileNum = mapTileNum[layerNr][worldCol][worldRow];

            int worldX = worldCol * TILE_WIDTH;
            int worldY = worldRow * TILE_HEIGHT;

            int screenX = worldX - player.worldX + player.screenX;
            int screenY = worldY - player.worldY + player.screenY;
            if(worldX >  player.worldX - player.screenX - TILE_WIDTH * 2 && worldX < player.worldX + player.screenX + TILE_WIDTH * 2 && worldY > player.worldY - player.screenY - TILE_HEIGHT * 2 && screenY <= player.screenY + TILE_HEIGHT/2 + 8) {
                if (tileNum != -1)
                    g.drawImage(tiles[tileNum].img.b, screenX, screenY, tiles[tileNum].width, tiles[tileNum].height, null);
                else
                    g.drawImage(tiles[0].img.b, screenX, screenY, tiles[0].width, tiles[0].height, null);

            }
            worldCol++;
            if (worldCol == gw.maxWolrdCol) {
                worldCol = 0;
                worldRow++;
            }
        }
    }
    public void DrawSecondHalf(Graphics g, Player player, int layerNr) {
        int worldCol = 0;
        int worldRow = 0;

        while (worldCol < gw.maxWolrdCol && worldRow < gw.maxWolrdRow) {
            int tileNum = mapTileNum[layerNr][worldCol][worldRow];

            int worldX = worldCol * TILE_WIDTH;
            int worldY = worldRow * TILE_HEIGHT;

            int screenX = worldX - player.worldX + player.screenX;
            int screenY = worldY - player.worldY + player.screenY;
            if(worldX >  player.worldX - player.screenX - TILE_WIDTH * 2 && worldX < player.worldX + player.screenX + TILE_WIDTH * 2 && screenY > player.screenY + TILE_HEIGHT/2 + 8 && worldY < (player.worldY + player.screenY + TILE_HEIGHT * 2)) {
                if (tileNum != -1)
                    g.drawImage(tiles[tileNum].img.b, screenX, screenY, tiles[tileNum].width, tiles[tileNum].height, null);
                else
                    g.drawImage(tiles[0].img.b, screenX, screenY, tiles[0].width, tiles[0].height, null);
            }
            worldCol++;

            if (worldCol == gw.maxWolrdCol) {
                worldCol = 0;
                worldRow++;
            }
        }
    }*/

    public BufferedImage DrawRotate(Graphics g, double angle , int screenX, int screenY) {

        double rads = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));
        int w = width;
        int h = height;
        int newWidth = (int) Math.floor(w * cos + h * sin);
        int newHeight = (int) Math.floor(h * cos + w * sin);

        BufferedImage rotated = new BufferedImage(newWidth / Tile.scale, newHeight / Tile.scale, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = rotated.createGraphics();
        AffineTransform at = new AffineTransform();


        int xRot = w / (2*Tile.scale);
        int yRot = h / (2*Tile.scale);

        at.translate((newWidth - w) / (2*Tile.scale), (newHeight - h) / (2*Tile.scale));
        at.rotate(rads, xRot, yRot);
        g2d.setTransform(at);
        g2d.drawImage(img.b, 0, 0, null);
        g2d.dispose();


        g.drawImage(rotated, screenX, screenY, newWidth, newHeight, null);
        return rotated;


    }
    /*! \fn public boolean IsSolid()
        \brief Returneaza proprietatea de dala solida (supusa coliziunilor) sau nu.
     */
    public boolean IsSolid()
    {
        return collision;
    }

    /*! \fn public int GetId()
        \brief Returneaza id-ul dalei.
     */
    public void setBox(int[] box)
    {
        img.box = box;
    }


    public void loadMaps(String[] filePath)
    {
        for(int i = 0; i < nrLayers; i++) {
            try {
                File myObj = new File(new File(filePath[i]).getAbsolutePath());
                Scanner myScanner = new Scanner(myObj);
                int col = 0;
                int row = 0;

                while (col < gw.maxWolrdCol && row < gw.maxWolrdRow) {
                    String line = myScanner.nextLine();

                    while (col < gw.maxWolrdCol) {
                        String[] nr = line.split(",");

                        int num = Integer.parseInt(nr[col]);
                        mapTileNum[i][col][row] = num;
                        col++;
                    }
                    if (col == gw.maxWolrdCol) {
                        col = 0;
                        row++;
                    }
                }
                myScanner.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
