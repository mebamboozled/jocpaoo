package PaooGame.Graphics;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import static PaooGame.Tiles.Tile.TILE_HEIGHT;
import static PaooGame.Tiles.Tile.TILE_WIDTH;

public class MyBuffImg {
    public BufferedImage b;
    public int box[];
    public String name = "";
    public String reference = "";
    public Rectangle solidArea;
    public MyBuffImg() {
        b = ImageLoader.LoadImage("/textures/missingSprite.png");
    }
    public MyBuffImg (BufferedImage img)
    {
        b = img;
        int[] box = {0,0,0,0};
        this.box=box;
        solidArea = new Rectangle(0, 0, TILE_WIDTH, TILE_HEIGHT);
    }
    public MyBuffImg (BufferedImage img, int[] box)
    {
        b = img;
        this.box=box;
        if(box.length == 9)
            solidArea = new Rectangle(box[5], box[6], box[7], box[8]);
        else
            solidArea = new Rectangle(0, 0, TILE_WIDTH, TILE_HEIGHT);
    }
    public MyBuffImg (MyBuffImg img)
    {
        b = img.b;
        box = img.box;
        name = img.name;
        reference = img.reference;
        solidArea = new Rectangle(img.solidArea);
    }

}






