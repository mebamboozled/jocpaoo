package PaooGame.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static PaooGame.Tiles.Tile.scale;

/*! \class public class SpriteSheet
    \brief Clasa retine o referinta catre o imagine formata din dale (sprite sheet)

    Metoda cropBox() returneaza o dala de dimensiuni fixe (o subimagine) din sprite sheet
    de la adresa (x * latimeDala, y * inaltimeDala)
 */
public class SpriteSheet
{
    private MyBuffImg       spriteSheet;              /*!< Referinta catre obiectul BufferedImage ce contine sprite sheet-ul.*/

    /*! \fn public SpriteSheet(BufferedImage sheet)
        \brief Constructor, initializeaza spriteSheet.

        \param img Un obiect BufferedImage valid.
     */
    public SpriteSheet(MyBuffImg buffImg)
    {
            /// Retine referinta catre BufferedImage object.
        spriteSheet = buffImg;
    }

    public SpriteSheet(BufferedImage loadImage) {
        int[] box = {0,0,0,0};
        spriteSheet= toBuffImg(loadImage);
        spriteSheet.box = box;
    }

    public MyBuffImg toBuffImg(BufferedImage img)
    {
        MyBuffImg temp = new MyBuffImg(img);
        return temp;
    }

    /*! \fn public BufferedImage cropBox(int x, int y)
        \brief Returneaza un obiect BufferedImage ce contine o subimage (dala).

        Subimaginea este localizata avand ca referinta punctul din stanga sus.

        \param x numarul dalei din sprite sheet pe axa x.
        \param y numarul dalei din sprite sheet pe axa y.
     */
    public BufferedImage cropBox(int box[])
    {
            /// Subimaginea (dala) este regasita in sprite sheet specificad coltul stanga sus
            /// al imaginii si apoi latimea si inaltimea (totul in pixeli). Coltul din stanga sus al imaginii
            /// se obtine inmultind numarul de ordine al dalei cu dimensiunea in pixeli a unei dale.

        return spriteSheet.b.getSubimage(box[0], box[1], box[2], box[3]);
    }

    public int[] getBox (String data, int frameIndex)
    {
        List<String> s = new ArrayList(Arrays.asList(data.split(" ")));
        int returns[] = new int[9];
        if (s.size()>=6)
            returns[4] = Integer.parseInt(s.get(5));
        returns[2] = Integer.parseInt(s.get(3));
        returns[3] = Integer.parseInt(s.get(4));
        returns[0] = Integer.parseInt(s.get(1)) + returns[2] * frameIndex;
        returns[1] = Integer.parseInt(s.get(2));
        if(s.size()==10)
        {
            returns[5] = Integer.parseInt(s.get(6)) * scale;
            returns[6] = Integer.parseInt(s.get(7)) * scale;
            returns[7] = Integer.parseInt(s.get(8)) * scale;
            returns[8] = Integer.parseInt(s.get(9)) * scale;
        }
        return returns;
    }

/*
    public MyBuffImg saveCrop(String title, int box[]) {
        MyBuffImg croppedImg = null;
        try {
            croppedImg = cropBox(box);
            System.out.println("ok: " + title);
        } catch (Exception e) {
            System.out.println("fail: " + title + " -- " + e.toString());
        }
        return croppedImg;
    }
*/

    public MyBuffImg crop(String data)
    {
        int numOfFrames;
        BufferedImage img = null;
        MyBuffImg bimg;
        List<String> arr = new ArrayList<String>(Arrays.asList(data.split(" ")));

        if(arr.size()==5) {
            img = cropBox(getBox(data, 0));
        }
        else if(arr.size()==6)
        {
            numOfFrames = Integer.parseInt(arr.get(5));
            for(int i = 0; i<= numOfFrames; i++)
            {
                img=cropBox(getBox(data, i));
            }
        }
        else if(arr.size() == 10)
        {
            numOfFrames = Integer.parseInt(arr.get(5));
            for(int i = 0; i<= numOfFrames; i++)
            {
                img=cropBox(getBox(data, i));
            }
        }
        bimg = new MyBuffImg(img, getBox(data, 0));
        bimg.name = arr.get(0);
        return bimg;
    }

    public MyBuffImg cropNoAnim(String data)
    {
        BufferedImage img = null;
        MyBuffImg bimg;
        List<String> arr = new ArrayList<String>(Arrays.asList(data.split(" ")));

        img = cropBox(getBox(data, 0));

        bimg = new MyBuffImg(img, getBox(data, 0));
        bimg.name = arr.get(0);
        return bimg;
    }
    public MyBuffImg crop(String data, int frame)
    {
        int numOfFrames;
        BufferedImage img = null;
        MyBuffImg bimg;
        List<String> arr = new ArrayList(Arrays.asList(data.split(" ")));
        if(arr.size()==5) {
            img = cropBox(getBox(data, 0));
        }
        else if(arr.size()==6)
        {
            numOfFrames = Integer.parseInt(arr.get(5));
            img=cropBox(getBox(data, frame % numOfFrames));
        }
        bimg = new MyBuffImg(img, getBox(data, 0));
        bimg.reference = data;
        return bimg;
    }

}



