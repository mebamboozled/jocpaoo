package PaooGame.Input;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class MouseHandler implements MouseListener, MouseMotionListener {

    public int x, y;
    @Override
    public void mouseDragged(MouseEvent e) {
        x = e.getXOnScreen();
        y = e.getYOnScreen();
    }
    @Override
    public void mouseMoved(MouseEvent e) {
        x = e.getXOnScreen();
        y = e.getYOnScreen();
    }
    @Override
    public void mouseClicked(MouseEvent me) {
        int screenX = me.getXOnScreen();
        int screenY = me.getYOnScreen();
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public int[] getCoord()
    {
        int [] coord = {x, y};
        if(coord == null)
            return new int[]{-1, -1};
        return coord;

    }
}