package PaooGame.Object;

import PaooGame.Tiles.Tile;

import java.awt.*;

import static PaooGame.Tiles.Tile.scale;

public class OBJ_Chest extends SuperObject {
    public OBJ_Chest(int idd) {
        Obj = new Tile(idd);
        name = Obj.img.name;
        image = Obj.img.b;
        id = idd;

        solidArea = new Rectangle();
        solidArea.height = 2 * scale;
        solidArea.width = Obj.width;
        solidArea.x = Obj.height - solidArea.width;
        solidArea.y = Obj.height - solidArea.height;
        collision = true;
    }
}