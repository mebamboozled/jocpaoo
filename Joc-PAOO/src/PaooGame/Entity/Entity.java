package PaooGame.Entity;

import PaooGame.Tiles.Tile;

import java.awt.*;

public abstract class Entity {

    public int worldX, worldY;
    public int screenX, screenY;
    public double acc, dacc;
    public int speed;
    public double speedX, speedY;
    public Tile texture;
    public Rectangle solidArea;
    public Rectangle solidAreaY;
    public Rectangle solidAreaX;
    public boolean collisionXOn = false;
    public boolean collisionYOn = false;
    public String directionX = "false";
    public String directionY = "false";

    public abstract Rectangle getBoundsX();
    public abstract Rectangle getBoundsY();

}
