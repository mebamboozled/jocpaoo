package PaooGame.Entity;

import PaooGame.GameWindow.GameWindow;
import PaooGame.Graphics.Assets;
import PaooGame.Input.MouseHandler;
import PaooGame.Tiles.Tile;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class Weapon extends Entity{
    double rotation;
    GameWindow gw;

    public static double centerX;
    public static double centerY;
    public double x = centerX + 16;
    public double y = centerY;
    public static int speed = 3;
    public static int R;
    public static int id;
    public static double angle;
    public Weapon(GameWindow gw, int idd) {
        id = idd;
        this.texture = new Tile(id);
        this.gw = gw;
        setDefaultValues();
    }

    public void setDefaultValues(){
        worldX = Tile.TILE_WIDTH * 23;
        worldY = Tile.TILE_WIDTH * 21;
        centerX = gw.GetWndWidth()/2 - Tile.TILE_WIDTH + texture.width/2;
        centerY = gw.GetWndHeight()/2 - Tile.TILE_HEIGHT;
        speed = 3;
        R = texture.height;
    }
    public void update()
    {
        double mouseX = MouseInfo.getPointerInfo().getLocation().x - gw.wndFrame.getX() ;
        double mouseY = MouseInfo.getPointerInfo().getLocation().y - gw.wndFrame.getY() - Tile.TILE_HEIGHT * 1.8;
        double[] coordMouse = new double[2];
        coordMouse = getColPoint(mouseX, mouseY);

        if(mouseY > centerY)
        {
            screenX = (int) (coordMouse[0] + centerX);
            screenY = (int) (coordMouse[1] + centerY);
        }
        else
        {
            screenX = (int) (coordMouse[0] + centerX);
            screenY = (int) (-coordMouse[1] + centerY);
        }
    }

    public void draw(Graphics g)
    {
        texture.DrawRotate(g, angle - 90, screenX - texture.width, screenY + texture.height/4);
    }




    public double[] getColPoint(double mouseX, double mouseY)
    {
        double[] coord = new double[2];
        double m = (centerY-mouseY)/(centerX-mouseX);
        double dy = centerY - mouseY;
        double dx = centerX - mouseX;
        double theta = Math.atan2(dy, dx);
        theta *= 180 / Math.PI;
        if(theta < 0)
            theta = 360 + theta;
        angle = theta;
        double deltaX;
        double deltaY;
        if(dy < 0) {
            if (m > 0) {

                deltaX = R * Math.cos(Math.atan(m));
                deltaY = R * Math.sin(Math.atan(m));
            } else {
                deltaX = -R * Math.cos(Math.atan(m));
                deltaY = -R * Math.sin(Math.atan(m));
            }
        }
        else
        {
            if (m > 0) {

                deltaX = - R * Math.cos(Math.atan(m));
                deltaY = R * Math.sin(Math.atan(m));
            } else {
                deltaX = R * Math.cos(Math.atan(m));
                deltaY =  - R * Math.sin(Math.atan(m));
            }
        }

        coord[0] = deltaX;
        coord[1] = deltaY;
        return coord;
    }
    public Rectangle getBoundsX()
    {
        Rectangle rec = solidAreaX;
        rec.x =(int) (screenX + speedX);
        rec.y = screenY;
        return rec;
    }
    public Rectangle getBoundsY()
    {
        Rectangle rec = solidAreaY;
        rec.x =screenX;
        rec.y = (int) (screenY - speedY);
        return rec;
    }

}
