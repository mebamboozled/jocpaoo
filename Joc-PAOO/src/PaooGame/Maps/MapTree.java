package PaooGame.Maps;

import javax.swing.tree.TreeNode;
import java.util.TreeSet;

public class MapTree{
    MapNode root;

    private MapNode addRecursive(MapNode current, MapNode future)
    {
        int primulNul = 0;
        if(current == null)
        {
            return new MapNode(future);
        }
        for(int i = 0; i < 4; i++)
        {
            if(current.Card[i] == null) {
                primulNul = i;
                break;
            }
        }
        addRecursive(current.Card[primulNul], future);
        return current;
    }

    public void add(MapNode future)
    {
        root = addRecursive(root, future);
    }

    public void traverseInOrder(MapNode node)
    {
        if(node != null)
        {
            for(int i = 0; i < 4; i++)
            {
                traverseInOrder(node.Card[i]);
                System.out.println(" " + node.mapId);
            }
        }
    }
}
