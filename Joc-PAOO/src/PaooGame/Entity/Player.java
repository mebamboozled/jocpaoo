package PaooGame.Entity;

import PaooGame.GameWindow.GameWindow;
import PaooGame.Graphics.Assets;
import PaooGame.Input.KeyHandler;
import PaooGame.Tiles.Tile;

import java.awt.*;

public class Player extends Entity {
    KeyHandler keyH;
    GameWindow gw;

    boolean moving = false;
    int frame = 0;
    int frameIdle = 0;
    int frameDash = 0;

    boolean lookingRight = true;

    boolean dashing = false;
    double dashSpeedMultiplier = 2;
    double[] speedPrev = new double[2];
    int dashTime = 30;
    int dashCooldown = dashTime + 60;

    int id;
    int frameCounter = 0;

    public Player(GameWindow gw, int idd) {
        id = idd;
        this.texture = new Tile(id);
        this.gw = gw;
        this.keyH = gw.keyH;
        screenX = gw.GetWndWidth()/2 - Tile.TILE_WIDTH;
        screenY = gw.GetWndHeight()/2 - Tile.TILE_HEIGHT;

        solidArea = new Rectangle();
        solidArea.width = (int) (texture.width * 0.8);
        solidArea.height = (int) ( texture.height * 0.2);
        solidArea.x = (texture.width - solidArea.width) / 2;
        solidArea.y = texture.height - solidArea.height;
        solidAreaY = solidArea;
        solidAreaX = solidArea;


        setDefaultValues();
    }

    public void setDefaultValues(){
        worldX = Tile.TILE_WIDTH * 23;
        worldY = Tile.TILE_WIDTH * 25;
        speed = 4;
        acc = 1;
        dacc = 0.5;
    }
    public void update(int thisFrame){
        double accTemp;
    directionY = "false";
    directionX = "false";
    if(frameCounter != 0) {
        gw.keyH.spacePressed = false;
        if (frameCounter == dashCooldown)
            frameCounter = 0;
    }

        speed = 4;
        if (gw.keyH.upPressed) {
            directionY = "up";
            moving = true;
        }
        if (gw.keyH.downPressed) {
            directionY = "down";
            moving = true;
        }
        if (gw.keyH.leftPressed) {
            directionX = "left";
            moving = true;
        }
        if (gw.keyH.rightPressed) {
            directionX = "right";
            moving = true;
        }
        if(gw.keyH.spacePressed && frameCounter == 0 && !dashing)
        {
            dashing = true;

            gw.keyH.spacePressed = false;
        }

/*        if(collisionSnap)
        {
            worldX = prevWorldX;
            worldY = prevWorldY;
        }
        if(!collisionSnap)
        {
            prevWorldX = worldX;
            prevWorldY = worldY;
        }*/

        if(directionY != "false" && directionX != "false")
            accTemp = acc / 1.43;
        else
            accTemp = acc;

        if(directionY == "false" && directionX == "false")
            accTemp = 0;

        if(true) {
            if (!collisionYOn && moving) {
                if (directionY == "up") {
                    speedY -= accTemp;
                }
                if (directionY == "down") {
                    speedY += accTemp;
                }
                if(directionY == "false")
                {
                    if(speedY > dacc)
                        speedY -= dacc;
                    else if(speedY < -dacc)
                        speedY += dacc;
                    else
                        speedY = 0;
                }
            }
            if (!collisionXOn && moving) {
                if (directionX == "left") {
                    speedX -= accTemp;
                    lookingRight = false;
                }
                if (directionX == "right") {
                    speedX += accTemp;
                    lookingRight = true;
                }
                if(directionX == "false")
                {
                    if(speedX > dacc)
                        speedX -= dacc;
                    else if(speedX < -dacc)
                        speedX += dacc;
                    else
                        speedX = 0;
                }
            }
            speedPrev[0] = speedX;
            speedPrev[1] = speedY;
        }
        speedX = clamp(speedX, -3, 3);
        speedY = clamp(speedY, -3, 3);

        collisionXOn = false;
        collisionYOn = false;

        gw.cChecker.checkTile(this);

        if(dashing && (speedX != 0 || speedY != 0))
        {
            if(!collisionXOn)
                worldX += speedPrev[0] * dashSpeedMultiplier;
            if(!collisionYOn)
                worldY += speedPrev[1] * dashSpeedMultiplier;
            frameCounter++;
            if(frameCounter == dashTime)
            {
                dashing = false;
                frameDash = 0;
            }
            dashAnimation(thisFrame);
        }
        else if(speedY != 0 || speedX != 0)
        {
            worldX += speedX;
            worldY += speedY;
            runAnimation(thisFrame);
        }
        else
        {
            frame = 0;
            moving = false;
        }
        if(collisionYOn || collisionXOn) {
            dashing = false;
            frameCounter = 0;
            frameDash = 0;
        }
        if(!moving)
        {
            idleAnimation(thisFrame);
        }
        else
        {
            frameIdle = 0;
        }
        if(frameCounter >= dashTime)
            frameCounter++;
    }
    public void draw(Graphics g){
        if(lookingRight)
            texture.Draw(g, screenX , screenY);
        else
            texture.DrawFlipped(g, screenX, screenY);
/*        g.drawRect(solidArea.x + screenX, solidArea.y + screenY, solidArea.width, solidArea.height);
        g.drawRect(snapArea.x + screenX, snapArea.y + screenY, snapArea.width, snapArea.height);*/
    }
    public void runAnimation(int thisFrame)
    {
        if(thisFrame % 5 == 0) {
            texture.img = Assets.sheet.crop(Tile.tiles[id + 1].img.reference, frame);
            frame++;
        }
    }
    public void dashAnimation(int thisFrame)
    {
        if(thisFrame % (dashTime / 4) == 0) {
            texture.img = Assets.sheet.crop(Tile.tiles[id + 3].img.reference, frameDash);
            frameDash++;
            if(frameDash > 4)
                frameDash = 4;
        }
    }
    public void idleAnimation(int thisFrame)
    {
        if(thisFrame % 10 == 0) {
            texture.img = Assets.sheet.crop(Tile.tiles[id].img.reference, frameIdle);
            frameIdle++;
        }
    }
    public double clamp(double speed, int min, int max)
    {
        if(speed > max)
            return max;
        else if(speed < min)
            return min;
        else return speed;
    }


    public void render(Graphics g)
    {
        g.setColor(Color.red);
        g.fillRect(getBoundsX().x, getBoundsX().y, (int) (solidAreaX.width), solidAreaX.height);

        g.setColor(Color.blue);
        g.fillRect(getBoundsY().x, getBoundsY().y, solidAreaY.width, (int) (solidAreaY.height));
    }

    public Rectangle getBoundsX()
    {
        Rectangle rec = solidAreaX;
        rec.x =(int) (screenX + speedX) + (texture.width - solidArea.width) / 2;
        rec.y = screenY + texture.height - solidArea.height;
        return rec;
    }
    public Rectangle getBoundsY()
    {
        Rectangle rec = solidAreaY;
        rec.x =screenX + (texture.width - solidArea.width) / 2;
        rec.y = (int) (screenY + speedY) + texture.height - solidArea.height;
        return rec;
    }
/*    public void render(Graphics g)
    {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setColor(Color.red);
        g2d.fill(getBoundsX());

        g2d.setColor(Color.blue);
        g2d.fill(getBoundsY());
    }*/
}

