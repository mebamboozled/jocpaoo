package PaooGame.Maps;

public class MapNode {
    int mapId;
    int noLayers;
    int width;
    int height;
    String mapName;
    MapNode[] Card = new MapNode[4];

    MapNode(){
        mapId = -1;
        noLayers = -1;
        width = -1;
        height = -1;
        mapName = "";
        for(MapNode i : Card)
        {
            i = null;
        }
    }
    MapNode(MapNode node){
        mapId = node.mapId;
        noLayers = node.noLayers;
        width = node.width;
        height = node.height;
        Card = node.Card;
        mapName = node.mapName;
    }
}
