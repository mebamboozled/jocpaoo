package PaooGame.Object;

import PaooGame.Tiles.Tile;

import java.io.IOException;

public class OBJ_Key extends SuperObject{
    public OBJ_Key(int idd)
    {
        Obj = new Tile(idd);
        name = Obj.img.name;
        image = Obj.img.b;
        id = idd;
    }
}
